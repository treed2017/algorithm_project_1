# Twelve Days of Christmas
# This script provides a class containing the phrases in the
# song, The Twelve Days of Christmas. Write a Python program to
# print out the song in its entirety

class TwelveDays:

    def __init__(self):
        self.phrase = ["On the ", "A partridge in a pear tree\n", "Two turtle doves\n", \
                    "Three french hens\n", "Four calling birds\n", "Five golden rings\n", \
                    "Six geese a-laying\n", "Seven swans a-swimming\n", "Eight maids a-milking\n", \
                    "Nine ladies dancing\n", "Ten lords a-leaping\n", "Eleven pipers piping\n", \
                    "Twelve drummers drumming\n", " day of Christmas, my true love gave to me\n"]

        self.ordinal = ["nothing", "first", "second", "third", \
                         "fourth", "fifth", "sixth", "seventh", \
                         "eighth", "ninth", "tenth", "eleventh", "twelfth"]


    def getGift(self, num):
        return self.phrase[num]

    def getFirstLine(self, num):
        return self.phrase[0] + self.ordinal[num] + self.phrase[13]